## Instructions on how to build a plugin from the source text 

The plugin consists of two parts - backend and frontend. 
To build the frontend part, you need to run the following commands in the source directory: 
To install dependencies: 

    yarn install

Build the plugin for production mode: 

    yarn build 

Build plugin in dev mode: 

    yarn dev 

There is also a developer mode that triggers frontend file tracking. 

It runs with a command: 

    yarn watch 

## Instructions on how to add the assembled plugin to Grafana 

There are many options for installing smartsheet-backend-plugin-rust; choose one of them: 

1. Installing the plugin using the grafana-cli utility: 

    grafana-cli --pluginUrl https://gitlab.com/can_solve_it/grafana/plugins/smartsheet-backend-plugin-rust/-/jobs/2912197435/artifacts/download plugins install smartsheet-backend-plugin-rust 

2. Installation is possible by copying files: 

Copy files to Grafana plugins directory  

http://docs.grafana.org/plugins/installation/#grafana-plugin-directory.  

Restart Grafana, check the list of data sources at http://your.grafana.instance/datasources/new and select Smartsheet 

3. Via helm chart: 

    plugins: 
        - https://gitlab.com/can_solve_it/grafana/plugins/smartsheet-backend-plugin-rust/-/jobs/2912197435/artifacts/download 
    grafana.ini: 
        plugins: 
            allow_loading_unsigned_plugins: smartsheet-backend-plugin-rust 

More information can be found here: https://github.com/grafana/helm-charts 

## Adding data source to Grafana 

Open the side menu by clicking the Grafana icon in the top header. 

1. In the side menu under Dashboards, you should find a link named Data Sources. 

2. Click the + Add data source button in the top header. 

3. Select Smartsheet from the Type dropdown. 

4. Paste in the Smartsheet API Token.  

NOTE: If you're not seeing the Data Sources link in your side menu,  it means that your current user does not have the Admin role for the current organization. 

