import { DataSourcePlugin } from '@grafana/data';
import { DataSource } from './datasource';
import { ConfigEditor } from './ConfigEditor';
import { QueryEditor } from './QueryEditor';
import { SmartsheetQuery, SmartsheetSourceOptions } from './types';

export const plugin = new DataSourcePlugin<DataSource, SmartsheetQuery, SmartsheetSourceOptions>(DataSource)
  .setConfigEditor(ConfigEditor)
  .setQueryEditor(QueryEditor);
