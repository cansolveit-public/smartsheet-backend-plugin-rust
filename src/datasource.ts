import { DataSourceInstanceSettings } from '@grafana/data';
import { DataSourceWithBackend } from '@grafana/runtime';
import { SmartsheetSourceOptions, SmartsheetQuery } from './types';

export class DataSource extends DataSourceWithBackend<SmartsheetQuery, SmartsheetSourceOptions> {
  constructor(instanceSettings: DataSourceInstanceSettings<SmartsheetSourceOptions>) {
    super(instanceSettings);
  }
}
