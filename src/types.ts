import { DataQuery, DataSourceJsonData } from '@grafana/data';

export interface SmartsheetQuery extends DataQuery {
  sheet_id?: string;
  withStreaming: boolean;
}

export const defaultQuery: Partial<SmartsheetQuery> = {
  withStreaming: false,
};

/**
 * These are options configured for each DataSource instance.
 */
export interface SmartsheetSourceOptions extends DataSourceJsonData {
  sheet_id?: string;
}

/**
 * Value that is used in the backend, but never sent over HTTP to the frontend
 */
export interface SmartsheetSecureJsonData {
  token?: string;
}
